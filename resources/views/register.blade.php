<!DOCTYPE html>
<html>
<title>Tugas 12 - Register</title>
<body>

<h1>Buat Account Baru!</h1>
<h3>Sign Up Form</h3>
<form action="/welcome" method="post">
@csrf
<p>First Name:</p>
<input type="text" id="fname" name="fname" value="">
<p>Last Name:</p>
<input type="text" id="lname" name="lname" value="">
<p>Gender:</p>
<input type="radio" id="male" name="gender" value="Male">
<label for="html">Male</label><br>
<input type="radio" id="female" name="gender" value="Female">
<label for="css">Female</label><br>
<input type="radio" id="other" name="gender" value="Other">
<label for="javascript">Other</label><br>
<p>Nationality:</p>
<select id="nationality">
  <option value="indonesia">Indonesia</option>
  <option value="malaysia">Malaysia</option>
  <option value="india">India</option>
  <option value="china">China</option>
</select>
<p>Language Spoken:</p>
<input type="checkbox" id="bahasa_indonesia" name="bahasa_indonesia">
<label for="language1"> Bahasa Indonesia</label><br>
<input type="checkbox" id="english" name="english">
<label for="language2"> English</label><br>
<input type="checkbox" id="other" name="other">
<label for="language3"> Other</label><br>
<p>Bio:</p>
<textarea id="bio" name="bio" rows="8" cols="20">
</textarea><br>
<input type="submit" value="kirim">
</form>
</body>
</html>